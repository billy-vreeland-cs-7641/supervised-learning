import logging

import pandas as pd

logging.basicConfig(format='%(levelname)s : %(asctime)s : %(message)s', level=logging.INFO)
logger = logging.getLogger()


class ClassifierPipeline:
    def __init__(self, filepath, verbose=True):
        self.filepath = filepath
        self.verbose = verbose

    def run(self):
        if self.verbose:
            logger.info('Beginning pipeline.')
        self._load_data()
        # Run data engineering
        # Run models
        # Generate metrics
        # Generate figures
        if self.verbose:
            logger.info('Pipeline completed.')

    def _load_data(self):
        df = pd.read_csv(self.filepath)

        if self.verbose:
            logger.info(f'Data from {self.filepath} loaded.')


