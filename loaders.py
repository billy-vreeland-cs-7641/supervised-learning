"""
Functions for loading data from files and performing processing steps.
"""

import re

import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder


def load_heart_disease_data(filepath):
    df = pd.read_csv(filepath)
    df = _format_column_names(df)
    categorical_cols = ('sex', 'chest_pain_type', 'resting_e_c_g', 'exercise_angina', 's_t__slope')
    for col in categorical_cols:
        df = _encode_categorical_column(df, col)
    return df


def load_beans_data(filepath):
    df = pd.read_csv('/home/billy/Downloads/DryBeanDataset/Dry_Bean_Dataset.csv', sep='\t')
    df = _format_column_names(df)
    df['class'] = df['class'].astype('category').cat.codes
    return df
    

def _format_column_names(df):
    df.columns = [re.sub(r'(?<!^)(?=[A-Z])', '_', col).lower() for col in df.columns]
    return df


def _encode_categorical_column(df, col):
    enc = OneHotEncoder(sparse=False, drop='first')
    enc_result = enc.fit_transform(df[[col]])
    for idx, cat in enumerate(enc.categories_[0][1:]):
        new_col = f'{col}_{cat.lower()}'
        df[new_col] = enc_result[:, idx].astype(int)
    df = df.drop(col, axis=1)
    return df

def filter_by_params(df, params):
    for k, v in params.items():
        col = f'param_{k}'
        df = df[df[col] == v]
    return df